terraform {
  source = "git::ssh://git@gitlab.com/fluffy-clouds-and-lines/dress4win-modules.git//vpc?ref=v0.0.1"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

inputs = {

}
