terraform {
  source = "git::ssh://git@gitlab.com/fluffy-clouds-and-lines/dress4win-modules.git//rds?ref=v0.0.1"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

inputs = {
  engine_version = "5.7.22",
  instance_size = "db.t2.micro"
  database_instance_name = "demodb"
  username = "superuser"
  password = "4qVXU7SbQ6nJj73k"
}

dependencies {
  paths = ["../vpc",]
}
